var webPage = require('webpage'); // for opening page
var fs = require('fs'); // for local file access

var page = webPage.create(),
    testindex = 0,
    loadInProgress = false;
var path = 'linkedin.html'; // tells script where to write the contents of page
var credentials = fs.read('credentials.txt').split('\n');

page.onConsoleMessage = function(msg) {
    console.log(msg);
};

page.onLoadStarted = function() {
    loadInProgress = true;
    console.log("load started");
};

page.onLoadFinished = function() {
    loadInProgress = false;
    console.log("load finished");

};

var steps = [
    function() {	// step 1
        //Load Login Page
        page.open("https://www.linkedin.com/");
    },
    function(credentials) {	//step 2
        //Enter Credentials
        page.evaluate(function(credentials) {

            var email = credentials[0];
            var password = credentials[1];

            var arr = document.getElementsByClassName("login-form");
            var i;

            for (i = 0; i < arr.length; i++) {
                if (arr[i].getAttribute('method') == "POST") {
                    arr[i].elements["login-email"].value = email;
                    arr[i].elements["login-password"].value = password;
                    return;
                }
            }
        }, credentials);
    },
    function() {	// step 3
        //Login
        page.evaluate(function() {
            var arr = document.getElementsByClassName("login-form");
            var i;

            for (i = 0; i < arr.length; i++) {
                if (arr[i].getAttribute('method') == "POST") {
                    arr[i].submit();
                    return;
                }
            }

        });
    },
    function() {	// step 4
    	// navigate to jobs page
        page.evaluate(function() {
			document.querySelector("a[href='/jobs/?trk=nav_responsive_sub_nav_jobs']").click();
        });
    },
    function() {	// step 5
        // Output content of page to file after form has been submitted
        page.onCallback = function(data) {
            if (!data.file) {
                data.file = "defaultFilename.txt";
            }
            if (!data.mode) {
                data.mode = "w";
            }
            fs.write(data.file, data.str, data.mode);
        };
        page.evaluate(function() {
            // save data
            if (typeof window.callPhantom === 'function') {
                window.callPhantom({ file: "linkedin.html", str: document.querySelectorAll('html')[0].outerHTML, mode: "w" }); // overwrite
            }
        });
    },
    function() {	// step 6
        // Output content of page to file after form has been submitted
        page.onCallback = function(data) {
            if (!data.file) {
                data.file = "defaultFilename.txt";
            }
            if (!data.mode) {
                data.mode = "w";
            }
            fs.write(data.file, data.str, data.mode);
        };
        page.evaluate(function() {
            // save data
            if (typeof window.callPhantom === 'function') {
            	var lis = document.querySelectorAll('li.item > a');

                window.callPhantom({ file: "items.html", str: "", mode: "w" }); // clear
            	for(var i = 0; i < lis.length; i++) {
                	window.callPhantom({ file: "items.html", str: lis[i].outerHTML, mode: "a" }); // append
            	}
            }
        });
    }
];

interval = setInterval(function() {
    if (!loadInProgress && typeof steps[testindex] == "function") {
        console.log("step " + (testindex + 1));
        steps[testindex](credentials);
        testindex++;
    }
    if (typeof steps[testindex] != "function") {
        console.log("test complete!");
        phantom.exit();
    }
}, 50);