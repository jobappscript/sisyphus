# README #

### What is this repository for? ###

* This repo is for development of the job application script using PhantomJS
* Version 0.0.0.0.4

### How do I get set up? ###

* must have node package manager installed
* install phantom `npm install -g phantomjs-prebuilt`
* run script `phantomjs open.js && open linkedin.html`

#### Next step ####

* create json file containing all job items with their details on the jobs page

#### Steps completed ####

* navigate to jobs page
* input credentials and sign in
* make text file for sensitive information and corresponding gitignore
* http://phantomjs.org/api/fs/method/write.html instead of `>>` in terminal

### Contribution guidelines ###

* Branch off of master branch to begin development